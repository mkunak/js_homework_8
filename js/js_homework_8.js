// Теоретический вопрос:
// Опишите своими словами, как Вы понимаете, что такое обработчик событий.
//
// Задание:
// Создать поле для ввода цены с валидацией.
//
// Технические требования:
// При загрузке страницы показать пользователю поле ввода (input) с надписью Price. Это поле будет служить для
// ввода числовых значений.
//
// Поведение поля должно быть следующим:
// При фокусе на поле ввода - у него должна появиться рамка зеленого цвета. При потере фокуса она пропадает.
// Когда убран фокус с поля - его значение считывается, над полем создается span, в котором должен быть
// выведен текст: Текущая цена: ${значение из поля ввода}. Рядом с ним должна быть кнопка с крестиком (X).
// Значение внутри поля ввода окрашивается в зеленый цвет.
// При нажатии на Х - span с текстом и кнопка X должны быть удалены. Значение, введенное в поле ввода, обнуляется.
// Если пользователь ввел число меньше 0 - при потере фокуса подсвечивать поле ввода красной рамкой, под
// полем выводить фразу - Please enter correct price. span со значением при этом не создается.
//
// В папке img лежат примеры реализации поля ввода и создающегося span.
//
// Литература:
// Поиск DOM элементов
// Добавление и удаление узлов
// Введение в браузерные события.
//
// Ответ на теоритический вопрос:
// Обработчик событий - это функция, которая запускается после того, как сработает данное событие.

function createElemAsChild(elementName, textInElement, elementParent) {
    let element = document.createElement(elementName);
    element.innerHTML = textInElement;
    return elementParent.appendChild(element);
}

const price = document.querySelector(`#price`);
const priceOutput = document.querySelector(`#priceOutput`);
const warningOutput = document.querySelector(`#warningOutput`);

price.addEventListener(`focus`, function () {
    price.value = ``;
    this.classList.add(`outlineGreen`);
    this.classList.add(`whiteField`);
});

price.addEventListener(`blur`, function () {
    if (!isNaN(this.value) && this.value !== `` && this.value > 0) {
        this.classList.remove(`whiteField`);
        this.classList.remove(`redField`);
        this.classList.add(`greenField`);
        const tdElement = createElemAsChild(`td`, ``, priceOutput);
        tdElement.classList.add(`whiteField`);
        let spanElement = createElemAsChild(`span`, `Текущая цена: ${this.value}`, tdElement);
        let btnElement = createElemAsChild(`button`, `X`, tdElement);
        btnElement.classList.add(`btnFont`);
        btnElement.addEventListener(`click`, function () {
            spanElement.remove();
            btnElement.remove();
            price.value = ``;
        });
    }
    if (isNaN(this.value) || this.value !== '' && this.value < 0) {
        while (priceOutput.hasChildNodes()) {
            priceOutput.firstChild.remove();
        }
        this.classList.remove(`greenField`);
        this.classList.remove(`whiteField`);
        this.classList.add(`redField`);
        const tdElement = createElemAsChild(`td`, `Please enter correct price`, warningOutput);
        tdElement.classList.add(`whiteField`);
        tdElement.classList.add(`position`);
        price.addEventListener('click', function () {
            tdElement.remove();
        });
    }
});